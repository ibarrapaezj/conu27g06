import { model, Schema, Types } from "mongoose";
import UsuarioEntidad from "../entidad/UsuarioEntidad";
// Aqui se colocan los nombres de la tabla = coleccion
// En esta linea se amarra el perfil esquema con el perfil entidad
const UsuarioEsquema = new Schema<UsuarioEntidad>(
  {
    nombreUsuario: { type: String, required: true, unique: true, trim: true },
    estadoUsuario: {type: Number, enum: [1,2,3], default: 1},
    correoUsuario: {type: String, unique:true, required: true, lowercase: true},
    claveUsuario: {type:String, required:true},
    fechaCreacionUsuario: {type: Date, default: Date.now()},
    codPerfil: {type: Types.ObjectId, ref: "Perfil", required: true}

  },
  { versionKey: false }
);

// Esta linea no permite que se le agregue al final una S a las colecciones
export default model("Usuario", UsuarioEsquema, "Usuario");