import { model, Schema } from "mongoose";
import PerfilEntidad from "../entidad/PerfilEntidad";
// Aqui se colocan los nombres de la tabla = coleccion
// En esta linea se amarra el perfil esquema con el perfil entidad
const PerfilEsquema = new Schema<PerfilEntidad>(
  {
    nombrePerfil: { type: String, required: true, unique: true, trim: true },
    estadoPerfil: {type: Number, enum: [1,2,3], default: 1}
  },
  { versionKey: false }
);

// Esta linea no permite que se le agregue al final una S a las colecciones
export default model("Perfil", PerfilEsquema, "Perfil");