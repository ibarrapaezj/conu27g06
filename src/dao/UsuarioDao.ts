import { Response } from "express";
import UsuarioEsquema from "../esquema/UsuarioEsquema";
import cifrado from "bcryptjs";
import jwt from "jsonwebtoken";

class UsuarioDao {
  // Creamos una promesa que saca los usuarios de DB
  protected static async consultarUsuarios(res: Response): Promise<any> {
    // En esta linea se hace una consulta
    const datos = await UsuarioEsquema.find().sort({ _id: -1 });
    // Se entrega la consulta al cliente
    res.status(200).json(datos);
  }

  protected static async crearUsuarios(correo: any, parametros: any, res: Response): Promise<any> {
    // En esta linea se verifica si el usuario ya existe
    const existe = await UsuarioEsquema.findOne(correo);
    if (existe) {
      res.status(400).json({ respuesta: "El correo ya existe" });
    } else {
      // Hacer el cifrado de la contraseña
      parametros.claveUsuario =cifrado.hashSync(parametros.claveUsuario, 10);

      const objUsuario = new UsuarioEsquema(parametros);
      objUsuario.save((miError, miObjeto) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede crear el usuario" });
        } else {
          const misDatos = {
            codUsuario: miObjeto._id,
            correo: parametros.correoUsuario
          };
          const miLlavecita = String(process.env.CLAVE_SECRETA);
          // Generar credenciales
          const miToken = jwt.sign(misDatos, miLlavecita, {expiresIn: 86400});
          res.status(200).json({
            token: miToken
          });
        };
      });
    }
  }

  protected static async eliminarUsuario(identificador: any,res: Response): Promise<any> {
    //const existe = await PerfilEsquema.findById(identificador);
    const existe = await UsuarioEsquema.findById(identificador).exec();
    if (existe) {
      UsuarioEsquema.findByIdAndDelete(
        identificador,
        (miError: any, miObjeto: any) => {
          if (miError) {
            res.status(400).json({ respuesta: "No se puede eliminar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Usuario eliminado exitosamente",
                eliminado: miObjeto,
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El usuario no existe" });
    }
  }

  protected static async actualizarUsuario(identificador: any,parametros: any,res: Response): Promise<any> {
    //const existe = await PerfilEsquema.findById(identificador);
    const existe = await UsuarioEsquema.findById(identificador).exec();
    if (existe) {
      UsuarioEsquema.findByIdAndUpdate(
        { _id: identificador },
        { $set: parametros },
        (miError: any, miObjeto: any) => {
          if (miError) {
            res.status(400).json({ respuesta: "No se puede actualizar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Usuario actualizado exitosamente",
                antes: miObjeto,
                despues: parametros
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El usuario no existe" });
    }
  }
}

export default UsuarioDao;
