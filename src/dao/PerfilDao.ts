import { Response } from "express";
import PerfilEsquema from "../esquema/PerfilEsquema";

class PerfilDao {
  // Creamos una promesa que saca los perfiles de DB
  protected static async consultarPerfiles(res: Response): Promise<any> {
    // En esta linea se hace una consulta
    const datos = await PerfilEsquema.find().sort({ _id: -1 });
    // Se entrega la consulta al cliente
    res.status(200).json(datos);
  }

  protected static async crearPerfiles(
    parametros: any,
    res: Response
  ): Promise<any> {
    // En esta linea se verifica si el perfil ya existe
    const existe = await PerfilEsquema.findOne(parametros);
    if (existe) {
      res.status(400).json({ respuesta: "El perfil ya existe" });
    } else {
      const objPerfil = new PerfilEsquema(parametros);
      objPerfil.save((miError, miObjeto) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede crear el perfil" });
        } else {
          res.status(200).json({
            respuesta: "Perfil creado exitosamente",
            codigo: miObjeto._id,
          });
        }
      });
    }
  }

  protected static async eliminarPerfil(
    identificador: any,
    res: Response
  ): Promise<any> {
    //const existe = await PerfilEsquema.findById(identificador);
    const existe = await PerfilEsquema.findById(identificador).exec();
    if (existe) {
      PerfilEsquema.findByIdAndDelete(
        identificador,
        (miError: any, miObjeto: any) => {
          if (miError) {
            res.status(400).json({ respuesta: "No se puede eliminar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Perfil eliminado exitosamente",
                eliminado: miObjeto,
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El perfil no existe" });
    }
  }

  protected static async actualizarPerfil(
    identificador: any,
    parametros: any,
    res: Response
  ): Promise<any> {
    //const existe = await PerfilEsquema.findById(identificador);
    const existe = await PerfilEsquema.findById(identificador).exec();
    if (existe) {
      PerfilEsquema.findByIdAndUpdate(
        { _id: identificador },
        { $set: parametros },
        (miError: any, miObjeto: any) => {
          if (miError) {
            res.status(400).json({ respuesta: "No se puede actualizar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Perfil actualizado exitosamente",
                antes: miObjeto,
                despues: parametros
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El perfil no existe" });
    }
  }
}

export default PerfilDao;
