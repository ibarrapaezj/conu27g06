import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";
import express from "express";
import conexionDB from "./ConexionDB"; // Se realiza la conexionDB
import apiPerfilRuta from "../ruta/PerfilRuta";
import apiUsuarioRuta from "../ruta/UsuarioRuta";
import seguridad from "../middleware/Seguridad";

class Servidor {
  // Definimos la variable para configuracion de todo lo que usamos en la api
  public app: express.Application;
  constructor() {
    // habilitar proyecto para usar variables de ambiente
    dotenv.config({ path: "variables.env" });
    // conectarse a mongo
    conexionDB();
    this.app = express();
    this.iniciarConfig();
    this.iniciarRutas();
  }

  public iniciarConfig() {
    // para asignar el puerto
    this.app.set("PORT", process.env.PORT);
    // bloquear y permitir acceso
    this.app.use(cors());
    // para que los mensajes salgan en la consola de desarrollo
    this.app.use(morgan("dev"));
    // Permite subir archivos hasta de 50MB
    this.app.use(express.json({ limit: "50MB" }));
    // para que la api resiva parametros  o consultas
    this.app.use(express.urlencoded({ extended: true }));
  }

  public iniciarRutas() {
    this.app.use("/api/perfil", seguridad.analizarToken, apiPerfilRuta);
    this.app.use("/api/Usuario", apiUsuarioRuta);
  }

  public iniciarServidor() {
    this.app.listen(this.app.get("PORT"), () => {
      console.log("Backen listo en el puerto", this.app.get("PORT"));
    });
  }
}

export default Servidor; // Permite usar la clase en cualquier parte del proyecto
