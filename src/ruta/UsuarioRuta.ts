//Para manejar ruteo
import { Router } from "express";

import usuarioControlador from "../controlador/UsuarioControlador";

class UsuarioRuta {
  //definimos una variable de tipo router
  public rutaApi: Router;
  // creamos el constructor para inicializar la variable
  constructor() {
    this.rutaApi = Router();
    this.configurarRutas();
  }
  //Creamos un metodo endpoint de tipo get
  public configurarRutas() {
    this.rutaApi.get("/listado", usuarioControlador.consulta);
    this.rutaApi.post("/crear", usuarioControlador.crear);
    this.rutaApi.delete("/eliminar/:codigo", usuarioControlador.eliminar);
    this.rutaApi.put("/actualizar/:codigo", usuarioControlador.actualizar);
  }
}

const usuarioruta = new UsuarioRuta();
export default usuarioruta.rutaApi;
