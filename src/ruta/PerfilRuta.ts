//Para manejar ruteo
import { Router } from "express";

import perfilControlador from "../controlador/PerfilControlador";

class PerfilRuta {
  //definimos una variable de tipo router
  public rutaApi: Router;
  // creamos el constructor para inicializar la variable
  constructor() {
    this.rutaApi = Router();
    this.configurarRutas();
  }
  //Creamos un metodo endpoint de tipo get
  public configurarRutas() {
    this.rutaApi.get("/listado", perfilControlador.consulta);
    this.rutaApi.post("/crear", perfilControlador.crear);
    this.rutaApi.delete("/eliminar/:codiguito", perfilControlador.eliminar);
    this.rutaApi.put("/actualizar/:codiguito", perfilControlador.actualizar);
  }
}

const perfilruta = new PerfilRuta();
export default perfilruta.rutaApi;
