import { PerfilEntidad } from './PerfilEntidad';
export class UsuarioEntidad {
  public nombreUsuario: string;
  public estadoUsuario: number;
  public correoUsuario: string;
  public claveUsuario: string;
  public fechaCreacionUsuario: Date;
  public codPerfil: PerfilEntidad;

  constructor(nomu: string, estu:number, coru:string, clavu: string, fechu: Date, codP: PerfilEntidad) {
    this.nombreUsuario = nomu;
    this.estadoUsuario = estu;
    this.correoUsuario = coru;
    this.claveUsuario = clavu;
    this.fechaCreacionUsuario = fechu;
    this.codPerfil = codP;
  };
};

export default UsuarioEntidad;
