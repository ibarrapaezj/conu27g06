import { Request, Response } from "express";
import UsuarioDao from "../dao/UsuarioDao";

class UsuarioControlador extends UsuarioDao {
  // Los controladores siempre deben tener 2 parametros
  public consulta(req: Request, res: Response) {
    UsuarioControlador.consultarUsuarios(res);
  }

  public crear(req: Request, res: Response) {
    const elCorreo ={correoUsuario: req.body.correoUsuario};
    UsuarioControlador.crearUsuarios(elCorreo, req.body, res);
  }

  public eliminar(req: Request, res: Response) {
    UsuarioControlador.eliminarUsuario(req.params.codigo, res);
  }

  public actualizar(req: Request, res: Response) {
    UsuarioControlador.actualizarUsuario(req.params.codigo, req.body, res);
  }
}

//
const usuarioControlador = new UsuarioControlador();

export default usuarioControlador;
