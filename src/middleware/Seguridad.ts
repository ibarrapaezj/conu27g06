// Este archivo bloquea o permite el acceso a las personas con token
import jwt  from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

class Seguridad {

    public analizarToken(req: Request, res: Response, next: NextFunction){
        if (req.headers.authorization) {
            try {
                const miLlavecita = String(process.env.CLAVE_SECRETA);
                const tokenRecibido = req.headers.authorization?.split(" ")[1] as string;
                const infoUsuario = jwt.verify(tokenRecibido, miLlavecita);
                req.body.datosUsuario = infoUsuario;
                next();
            } catch (error) {
                res.status(401).json({respuesta:"Pal cai por falsificador"});
            }
        } else {
            res.status(401).json({respuesta:"Bolillaso y pa fuera..."});
        }
    };
}

const seguridad = new Seguridad();
export default seguridad;
