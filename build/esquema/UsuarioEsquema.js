"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
// Aqui se colocan los nombres de la tabla = coleccion
// En esta linea se amarra el perfil esquema con el perfil entidad
const UsuarioEsquema = new mongoose_1.Schema({
    nombreUsuario: { type: String, required: true, unique: true, trim: true },
    estadoUsuario: { type: Number, enum: [1, 2, 3], default: 1 },
    correoUsuario: { type: String, unique: true, required: true, lowercase: true },
    claveUsuario: { type: String, required: true },
    fechaCreacionUsuario: { type: Date, default: Date.now() },
    codPerfil: { type: mongoose_1.Types.ObjectId, ref: "Perfil", required: true }
}, { versionKey: false });
// Esta linea no permite que se le agregue al final una S a las colecciones
exports.default = (0, mongoose_1.model)("Usuario", UsuarioEsquema, "Usuario");
