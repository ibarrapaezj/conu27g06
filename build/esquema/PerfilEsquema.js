"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
// Aqui se colocan los nombres de la tabla = coleccion
// En esta linea se amarra el perfil esquema con el perfil entidad
const PerfilEsquema = new mongoose_1.Schema({
    nombrePerfil: { type: String, required: true, unique: true, trim: true },
    estadoPerfil: { type: Number, enum: [1, 2, 3], default: 1 }
}, { versionKey: false });
// Esta linea no permite que se le agregue al final una S a las colecciones
exports.default = (0, mongoose_1.model)("Perfil", PerfilEsquema, "Perfil");
