"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const ConexionDB_1 = __importDefault(require("./ConexionDB")); // Se realiza la conexionDB
const PerfilRuta_1 = __importDefault(require("../ruta/PerfilRuta"));
const UsuarioRuta_1 = __importDefault(require("../ruta/UsuarioRuta"));
const Seguridad_1 = __importDefault(require("../middleware/Seguridad"));
class Servidor {
    constructor() {
        // habilitar proyecto para usar variables de ambiente
        dotenv_1.default.config({ path: "variables.env" });
        // conectarse a mongo
        (0, ConexionDB_1.default)();
        this.app = (0, express_1.default)();
        this.iniciarConfig();
        this.iniciarRutas();
    }
    iniciarConfig() {
        // para asignar el puerto
        this.app.set("PORT", process.env.PORT);
        // bloquear y permitir acceso
        this.app.use((0, cors_1.default)());
        // para que los mensajes salgan en la consola de desarrollo
        this.app.use((0, morgan_1.default)("dev"));
        // Permite subir archivos hasta de 50MB
        this.app.use(express_1.default.json({ limit: "50MB" }));
        // para que la api resiva parametros  o consultas
        this.app.use(express_1.default.urlencoded({ extended: true }));
    }
    iniciarRutas() {
        this.app.use("/api/perfil", Seguridad_1.default.analizarToken, PerfilRuta_1.default);
        this.app.use("/api/Usuario", UsuarioRuta_1.default);
    }
    iniciarServidor() {
        this.app.listen(this.app.get("PORT"), () => {
            console.log("Backen listo en el puerto", this.app.get("PORT"));
        });
    }
}
exports.default = Servidor; // Permite usar la clase en cualquier parte del proyecto
