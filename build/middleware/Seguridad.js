"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Este archivo bloquea o permite el acceso a las personas con token
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class Seguridad {
    analizarToken(req, res, next) {
        var _a;
        if (req.headers.authorization) {
            try {
                const miLlavecita = String(process.env.CLAVE_SECRETA);
                const tokenRecibido = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(" ")[1];
                const infoUsuario = jsonwebtoken_1.default.verify(tokenRecibido, miLlavecita);
                req.body.datosUsuario = infoUsuario;
                next();
            }
            catch (error) {
                res.status(401).json({ respuesta: "Pal cai por falsificador" });
            }
        }
        else {
            res.status(401).json({ respuesta: "Bolillaso y pa fuera..." });
        }
    }
    ;
}
const seguridad = new Seguridad();
exports.default = seguridad;
