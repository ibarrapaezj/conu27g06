"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PerfilEntidad = void 0;
class PerfilEntidad {
    constructor(nomp, estp) {
        this.nombrePerfil = nomp;
        this.estadoPerfil = estp;
    }
    ;
}
exports.PerfilEntidad = PerfilEntidad;
;
exports.default = PerfilEntidad;
