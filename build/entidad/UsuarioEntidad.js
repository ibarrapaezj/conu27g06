"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioEntidad = void 0;
class UsuarioEntidad {
    constructor(nomu, estu, coru, clavu, fechu, codP) {
        this.nombreUsuario = nomu;
        this.estadoUsuario = estu;
        this.correoUsuario = coru;
        this.claveUsuario = clavu;
        this.fechaCreacionUsuario = fechu;
        this.codPerfil = codP;
    }
    ;
}
exports.UsuarioEntidad = UsuarioEntidad;
;
exports.default = UsuarioEntidad;
