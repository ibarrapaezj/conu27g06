"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioEsquema_1 = __importDefault(require("../esquema/UsuarioEsquema"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class UsuarioDao {
    // Creamos una promesa que saca los usuarios de DB
    static consultarUsuarios(res) {
        return __awaiter(this, void 0, void 0, function* () {
            // En esta linea se hace una consulta
            const datos = yield UsuarioEsquema_1.default.find().sort({ _id: -1 });
            // Se entrega la consulta al cliente
            res.status(200).json(datos);
        });
    }
    static crearUsuarios(correo, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // En esta linea se verifica si el usuario ya existe
            const existe = yield UsuarioEsquema_1.default.findOne(correo);
            if (existe) {
                res.status(400).json({ respuesta: "El correo ya existe" });
            }
            else {
                // Hacer el cifrado de la contraseña
                parametros.claveUsuario = bcryptjs_1.default.hashSync(parametros.claveUsuario, 10);
                const objUsuario = new UsuarioEsquema_1.default(parametros);
                objUsuario.save((miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede crear el usuario" });
                    }
                    else {
                        const misDatos = {
                            codUsuario: miObjeto._id,
                            correo: parametros.correoUsuario
                        };
                        const miLlavecita = String(process.env.CLAVE_SECRETA);
                        // Generar credenciales
                        const miToken = jsonwebtoken_1.default.sign(misDatos, miLlavecita, { expiresIn: 86400 });
                        res.status(200).json({
                            token: miToken
                        });
                    }
                    ;
                });
            }
        });
    }
    static eliminarUsuario(identificador, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //const existe = await PerfilEsquema.findById(identificador);
            const existe = yield UsuarioEsquema_1.default.findById(identificador).exec();
            if (existe) {
                UsuarioEsquema_1.default.findByIdAndDelete(identificador, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede eliminar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Usuario eliminado exitosamente",
                            eliminado: miObjeto,
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El usuario no existe" });
            }
        });
    }
    static actualizarUsuario(identificador, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //const existe = await PerfilEsquema.findById(identificador);
            const existe = yield UsuarioEsquema_1.default.findById(identificador).exec();
            if (existe) {
                UsuarioEsquema_1.default.findByIdAndUpdate({ _id: identificador }, { $set: parametros }, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede actualizar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Usuario actualizado exitosamente",
                            antes: miObjeto,
                            despues: parametros
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El usuario no existe" });
            }
        });
    }
}
exports.default = UsuarioDao;
