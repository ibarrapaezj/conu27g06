"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//Para manejar ruteo
const express_1 = require("express");
const UsuarioControlador_1 = __importDefault(require("../controlador/UsuarioControlador"));
class UsuarioRuta {
    // creamos el constructor para inicializar la variable
    constructor() {
        this.rutaApi = (0, express_1.Router)();
        this.configurarRutas();
    }
    //Creamos un metodo endpoint de tipo get
    configurarRutas() {
        this.rutaApi.get("/listado", UsuarioControlador_1.default.consulta);
        this.rutaApi.post("/crear", UsuarioControlador_1.default.crear);
        this.rutaApi.delete("/eliminar/:codigo", UsuarioControlador_1.default.eliminar);
        this.rutaApi.put("/actualizar/:codigo", UsuarioControlador_1.default.actualizar);
    }
}
const usuarioruta = new UsuarioRuta();
exports.default = usuarioruta.rutaApi;
